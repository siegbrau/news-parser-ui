import {Component, OnInit, ViewChild} from '@angular/core';
import {NewsService} from '../services/news.service';
import {News} from '../model/News';
import {MatPaginator, PageEvent} from '@angular/material/paginator';

import {interval} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

// TODO https://medium.com/bb-tutorials-and-thoughts/retaining-state-of-the-angular-app-when-page-refresh-with-ngrx-6c486d3012a9
@Component({
  selector: 'app-main-feed',
  templateUrl: './main-feed.component.html',
  styleUrls: ['./main-feed.component.css']
})
export class MainFeedComponent implements OnInit {
  newsfeed: News[];
  isLoading = true;
  hiddenList: Array<News> = [];
  defaultNews: News = {id: 99999, title: 'default', tagList: [], source: 'source', link: 'link' };
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource;
  lowValue = 0;
  highValue = 20;
  timeDelay = 10000;

  constructor(private newsService: NewsService, private reminder: MatSnackBar) {
  }

  ngOnInit(): void {
    this.defaultFeed();
    console.log(this.hiddenList);
    this.dataSource.paginator = this.paginator;
  }

  // Default feed which is used on start if no search options is chosen
  public defaultFeed(): void {
    interval(this.timeDelay).subscribe(val => {
      this.newsService
        .getAllNews()
        .subscribe(allNews => {
          this.newsfeed = allNews;
          this.dataSource = allNews;
          this.isLoading = false;
          this.reminder.open('News feed is refreshed', 'Close',
            {
              duration: 1500,
              direction: 'ltr',
              verticalPosition: 'top',
              horizontalPosition: 'right',
            });
        });

    });
  }

// used to build an array of papers relevant at any given time
  public getPaginatorData(event: PageEvent): PageEvent {
    this.lowValue = event.pageIndex * event.pageSize;
    this.highValue = this.lowValue + event.pageSize;
    return event;
  }

  hideNews(id: string, news: News): void {
    document.getElementById(id).style.display = 'none';
    this.hiddenList.push(news);
  }
}
