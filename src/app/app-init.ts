import { KeycloakService } from 'keycloak-angular';
import {environment} from '../environments/environment';

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise<any>(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: environment.keycloak,
          enableBearerInterceptor: true,
          bearerExcludedUrls: ['/', '/home', '/asset']
        });
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  };
}
