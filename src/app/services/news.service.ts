import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {News} from '../model/News';
import {KeycloakService} from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private urlPrefix = 'http://localhost:8080/news/';
  private urlSource = this.urlPrefix + 'source/';
  private urlTopic = this.urlPrefix + 'topic/';

  // TODO: добавить header  к запросу https://stackoverflow.com/questions/34464108/angular-set-headers-for-every-request
  constructor(private http: HttpClient, private keycloakService: KeycloakService) {
  }
  getAllNews(): Observable<News[]> {
    return this.http.get<News[]>(`${this.urlSource}` + 'all');
  }
  getBySourceNews(source: any): Observable<News[]> {
    return this.http.get<News[]>(`${this.urlSource}` + source );
  }
  getByTopicNews(topic: string): Observable<News[]> {
    return this.http.get<News[]>(`${this.urlTopic}` + topic );
  }
}
