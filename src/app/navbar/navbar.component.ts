import {Component, OnInit, Output} from '@angular/core';
import {NewsService} from '../services/news.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output() searchRequest: string;

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
  }
  public searchRequestSet(text: string): void {
    this.searchRequest = text;
  }

}
