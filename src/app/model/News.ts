export interface News {
  id: number;
  title: string;
  link: string;
  source: any;
  tagList: Array<any>;
}
